package com.example.cotizacionautos;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class CotizacionActivity extends AppCompatActivity {

    private Cotizacion cotizacion;
    private TextView lblfolio;
    private TextView lblCliente;
    private TextView lblEnganche;
    private TextView lblPagoMes;
    private EditText txtDesc;
    private EditText txtPorcen;
    private EditText txtVal;
    private RadioButton rbtnP1;
    private RadioButton rbtnP2;
    private RadioButton rbtnP3;
    private RadioButton rbtnP4;
    private Button btnReg;
    private Button btnCal;
    private Button btnLim;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cotizacion);

        lblCliente = (TextView) findViewById(R.id.lblCliente);
        lblfolio = (TextView) findViewById(R.id.lblFolio);
        lblEnganche = (TextView) findViewById(R.id.lblEnganche);
        lblPagoMes = (TextView) findViewById(R.id.lblPagoMes);
        txtVal = (EditText) findViewById(R.id.txtValor);
        txtPorcen = (EditText) findViewById(R.id.txtPorcentaje);
        txtDesc = (EditText) findViewById(R.id.txtDescripcion);
        rbtnP1 = (RadioButton) findViewById(R.id.rbtnP1);
        rbtnP2 = (RadioButton) findViewById(R.id.rbtnP2);
        rbtnP3 = (RadioButton) findViewById(R.id.rbtnP3);
        rbtnP4 = (RadioButton) findViewById(R.id.rbtnP4);
        btnReg = (Button) findViewById(R.id.btnSalir);
        btnCal = (Button) findViewById(R.id.btnCalcular);
        btnLim = (Button) findViewById(R.id.btnLimpiar);

        //Sacar el dato String
        Bundle datos = getIntent().getExtras();
        String cliente = datos.getString("cliente");
        lblCliente.setText("Cliente: " + cliente);

        //Sacar el Objeto
        cotizacion = (Cotizacion) datos.getSerializable("cotizacion");
        String Folio = String.valueOf(cotizacion.getFolio());
        lblfolio.setText("Folio: " + Folio);

        btnCal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(txtVal.getText().toString().matches("")){
                    Toast.makeText(CotizacionActivity.this,"Debes llenar todos los campos para calcular",Toast.LENGTH_SHORT).show();
                }
                else if(txtPorcen.getText().toString().matches("")){
                    Toast.makeText(CotizacionActivity.this,"Debes llenar todos los campos para calcular",Toast.LENGTH_SHORT).show();
                }
                else if(rbtnP1.isChecked() == false && rbtnP2.isChecked() == false && rbtnP3.isChecked() == false && rbtnP4.isChecked() == false){
                    Toast.makeText(CotizacionActivity.this,"Debes seleccionar un plazo para calcular",Toast.LENGTH_SHORT).show();
                }
                else{
                    String Cos = txtVal.getText().toString();
                    float Costo = Float.parseFloat(Cos);
                    String Por = txtPorcen.getText().toString();
                    float Porcentaje = Float.parseFloat(Por);

                    cotizacion.setValorAuto(Costo);
                    cotizacion.setPorEnganche(Porcentaje);

                    String Enganche = String.valueOf(cotizacion.calculoPagoInicial());
                    lblEnganche.setText("Enganche: $" + Enganche);

                    if(rbtnP1.isChecked() == true){
                        cotizacion.setPlazos(1);
                    }else if(rbtnP2.isChecked() == true){
                        cotizacion.setPlazos(2);
                    }else if(rbtnP3.isChecked() == true){
                        cotizacion.setPlazos(3);
                    }else if(rbtnP4.isChecked() == true){
                        cotizacion.setPlazos(4);
                    }

                    String PagoMes = String.valueOf(cotizacion.calculoPagoMensual());
                    lblPagoMes.setText("Pago mensual es: $" + PagoMes);
                }
            }
        });

        btnLim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rbtnP1.setChecked(false);
                rbtnP2.setChecked(false);
                rbtnP3.setChecked(false);
                rbtnP4.setChecked(false);
                txtDesc.setText("");
                txtVal.setText("");
                txtPorcen.setText("");
                lblEnganche.setText("");
                lblPagoMes.setText("");
            }
        });

        btnReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}
