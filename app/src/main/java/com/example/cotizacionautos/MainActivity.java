package com.example.cotizacionautos;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.Serializable;

public class MainActivity extends AppCompatActivity {

    private Cotizacion cotizacion;
    private EditText txtNombre;
    private Button btnEnviar;
    private Button btnCerrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtNombre = (EditText) findViewById(R.id.txtCliente);
        btnEnviar = (Button) findViewById(R.id.btnIngresar);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);
        cotizacion = new Cotizacion();

        btnEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cotizacion.setFolio(cotizacion.generaFolio());
                String cliente = txtNombre.getText().toString();

                if(cliente.matches("")){
                    Toast.makeText(MainActivity.this,"Falto capturar nombre",Toast.LENGTH_SHORT).show();
                }
                else{
                    Intent intent = new Intent(MainActivity.this,CotizacionActivity.class);
                    intent.putExtra("cliente",cliente); ////Para enviar dato a otra actividad
                    Bundle objeto = new Bundle();   ////Para enviar objetos
                    objeto.putSerializable("cotizacion", cotizacion);

                    intent.putExtras(objeto);

                    startActivity(intent);
                }
            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}
