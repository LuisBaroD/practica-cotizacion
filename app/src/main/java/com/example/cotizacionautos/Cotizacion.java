package com.example.cotizacionautos;

import java.io.Serializable;
import java.util.Random;

public class Cotizacion implements Serializable {

    private int folio;
    private String descripcion;
    private float valorAuto;
    private float porEnganche;
    private int plazos;

    public Cotizacion(int folio, String descripcion, float valorAuto, float porEnganche, int plazos) {
        this.folio = folio;
        this.descripcion = descripcion;
        this.valorAuto = valorAuto;
        this.porEnganche = porEnganche;
        this.plazos = plazos;
    }

    public Cotizacion() {

    }

    public int getFolio() {
        return folio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public float getValorAuto() {
        return valorAuto;
    }

    public float getPorEnganche() {
        return porEnganche;
    }

    public int getPlazos() {
        return plazos;
    }

    public void setFolio(int folio) {
        this.folio = folio;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setValorAuto(float valorAuto) {
        this.valorAuto = valorAuto;
    }

    public void setPorEnganche(float porEnganche) {
        this.porEnganche = porEnganche;
    }

    public void setPlazos(int plazos) {
        this.plazos = plazos;
    }

    public int generaFolio (){
        return new Random().nextInt()%1000;
    }

    public float calculoPagoInicial(){
        float pago = this.valorAuto * (this.porEnganche)/100;
        return pago;
    }

    public float calculoPagoMensual(){
        float totalFin = this.valorAuto - this.calculoPagoInicial();
        int plazo = 0;
        switch(this.plazos){
            case 1: plazo = 12; break;
            case 2: plazo = 18; break;
            case 3: plazo = 24; break;
            case 4: plazo = 36; break;
        }
        return totalFin / plazo;
    }
}
